#!/bin/bash

CONTAINERNAME=mkdocs-material
IMAGENAME=ekleinod/$CONTAINERNAME

echo "Serve documentation at http://localhost:8000/."
echo

docker run \
	--rm \
	--name $CONTAINERNAME \
	--user "$(id -u):$(id -g)" \
	--interactive \
	--tty \
	--volume "${PWD}:/docs" \
	--publish 8000:8000 \
	$IMAGENAME \
		$@
