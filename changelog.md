# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [2.0.0] 2014-01-16

### Added

- flattening of substructures #3

### Changed

- converted root element from array to object #2


## [1.0.0] 2024-01-15

- initial version

### Added

- script for todo.txt -> json conversion #1
