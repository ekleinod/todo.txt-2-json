# todo.txt 2 json

This is a python script to convert todo.txt to a json representation.

- [manual](https://ekleinod.gitlab.io/todo.txt-2-json)
- [issues](https://gitlab.com/ekleinod/todo.txt-2-json/-/issues)
- [changelog](changelog.md)

## Copyright

Copyright 2023-2023 Ekkart Kleinod <ekleinod@edgesoft.de>

This script is free software: you can redistribute them and/or modify
them under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This script is distributed in the hope that they will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with This script.  If not, see <https://www.gnu.org/licenses/>.
