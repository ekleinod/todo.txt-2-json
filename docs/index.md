# todo.txt 2 json

![edge-soft logo](assets/icon.png){ align=right }

!!! tip ""
	Use your ToDos in other contexts!

This is a python script to convert [todo.txt](http://todotxt.org) to a json representation.

This eases the use of ToDos in other contexts that can work with json easier.

- [issues](https://gitlab.com/ekleinod/todo.txt-2-json/-/issues)

The script uses the [pytodotxt library](https://github.com/vonshednob/pytodotxt).

## Format

For examples, see [test directory](https://gitlab.com/ekleinod/todo.txt-2-json/-/tree/main/python/test).

For todo.txt see [format description](https://github.com/todotxt/todo.txt)

The generated json format is (only *description* is mandatory):

~~~ json
{
	"todos": [
		{
			"is_completed": true,
			"priority": "priority",
			"completion_date": "yyyy-mm-dd",
			"creation_date": "yyyy-mm-dd",
			"description": "description",
			"projects": [
				"project1",
				"project2"
			],
			"contexts": [
				"context1",
				"context2"
			],
			"attributes": {
				"key": [
					"value"
				]
			},
		},
		{ ... }
	]
}
~~~

## Python script

Use the script as follows:

~~~ bash
$> python todo.txt-2-json.py --input <infile> --output <outfile> [--flatten] [--verbose]
~~~

`--input`
: input file

`--output`
: output file

`--flatten`
: flatten substructures

`--verbose`
: verbose output


## Shell script

Use the script as follows:

~~~ bash
$> todo.txt-2-json.sh -i <infile> -o <outfile> [-f] [-v] [-h]
~~~

`-i`
: input file

`-o`
: output file

`-f`
: flatten substructures

`-v`
: verbose output

`-h`
: script help


## License

This script is distributed under the terms of the GNU General Public License.

See [readme](https://gitlab.com/ekleinod/todo.txt-2-json/-/blob/main/README.md) or [COPYING](https://gitlab.com/ekleinod/todo.txt-2-json/-/blob/main/COPYING) for details.
