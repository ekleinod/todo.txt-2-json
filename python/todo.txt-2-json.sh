# This file is part of todo.txt-2-json.
# license: LGPL, see COPYING and COPYING.LESSER
# Ekkart Kleinod (https://www.edgesoft.de/)

echo "Converts todo.txt to json."
echo

# help
function showHelp() {
	echo "NAME"
	echo -e "\ttodo.txt-2-json - converts todo.txt to json"
	echo
	echo "SYNOPSIS"
	echo -e "\ttodo.txt-2-json -i TODO-FILE [-o OUT-FILE] [-f] [-v]"
	echo
	echo "DESCRIPTION"
	echo -e "\tThis script converts the todo.txt file a json file."
	echo
	echo -e "\t-i\ttodo.txt file"
	echo -e "\t-o\toutput file (default: infile with extension .json)"
	echo -e "\t-f\tflatten substructures"
	echo -e "\t-v\tverbose output"
	echo -e "\t-h\tshow this help"
	echo
}

VERBOSE=""
FLATTEN=""

# parameters
while getopts hi:o:fv option
do
	case "${option}"
		in
			i)
				infile=${OPTARG}
				;;
			o)
				outfile=${OPTARG}
				;;
			f)
				FLATTEN="--flatten"
				;;
			v)
				VERBOSE="--verbose"
				;;
			h | *)
				showHelp
				exit 0
				;;
	esac
done

EXTENSION_IN=.txt
EXTENSION_OUT=.json

# mandatory arguments
if [ -z "$infile" ]
then
	echo "mandatory option -i (todo.txt file) is missing"
	echo
	showHelp
	exit 0
fi

# optional arguments
if [ -z "$outfile" ]
then
	outfile=$infile
	if [ `expr "$infile" : ".*\$EXTENSION_IN"` != "0" ]
	then
		outfile=`expr "$outfile" : "\(.*\)\$EXTENSION_IN"`
	fi
	outfile="$outfile$EXTENSION_OUT"
fi

CONTAINER_PYTHON=python
IMAGE_PYTHON=ekleinod/$CONTAINER_PYTHON
WORKDIR_PYTHON=/usr/src/myapp

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--interactive \
	--tty \
	--name $CONTAINER_PYTHON \
	--volume "${PWD}":$WORKDIR_PYTHON \
	--workdir $WORKDIR_PYTHON \
	$IMAGE_PYTHON \
		todo.txt-2-json/todo.txt-2-json.py \
			--input $infile \
			--output $outfile \
			$FLATTEN \
			$VERBOSE
