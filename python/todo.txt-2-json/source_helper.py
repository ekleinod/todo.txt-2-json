# This file is part of todo.txt-2-json.
# license: LGPL, see COPYING and COPYING.LESSER
# Ekkart Kleinod (https://www.edgesoft.de/)

import json
import os

# read json file
def read_json_file(filename, depth = -1, is_verbose = False):

	if (is_verbose):
		print("{}reading from '{}'".format("\t" * (depth), filename))

	input_file = os.path.join(os.getcwd(), filename)

	if not os.path.isfile(input_file):
		raise FileNotFoundError("File '{}' does not exist.".format(input_file))

	if (is_verbose):
		print("{}which is '{}'".format("\t" * (depth + 1), input_file))

	with open(input_file, 'r') as file:
		json_raw = json.load(file)

	if (is_verbose):
		print("{}read json content successfully.".format("\t" * (depth + 1)))

	return json_raw

# save json file
def save_json_file(json_data, filename, depth = -1, is_verbose = False):

	if (is_verbose):
		print("{}saving to '{}'".format("\t" * (depth + 1), filename))
	os.makedirs(os.path.dirname(filename), exist_ok=True)
	with open(filename, 'w', encoding='utf-8') as outfile:
		json.dump(json_data, outfile, sort_keys=True, indent=4)

	return
