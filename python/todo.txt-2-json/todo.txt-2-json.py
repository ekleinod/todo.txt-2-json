# This file is part of todo.txt-2-json.
# license: LGPL, see COPYING and COPYING.LESSER
# Ekkart Kleinod (https://www.edgesoft.de/)

import argparse
import json
import logging
import os
import pytodotxt
import sys

from source_helper import save_json_file

KEY_EMPTY = "none"

# load todo
def load_todotxt(input_file):

	if os.path.isfile(input_file):

		logging.info("Loading '%s'.", input_file)

		todos = pytodotxt.TodoTxt(input_file)
		todos.parse()

	else:
		raise FileNotFoundError("File '{}' does not exist or is not a file.".format(input_file))

	return todos

# convert todos to json
def todotxt2json(todos, flatten):

	tasks_json = {}

	tasks_array = []
	for task in todos.tasks:
		tasks_array.append(task2map(task))
	tasks_json["todos"] = tasks_array

	if (flatten):
		tasks_json["todos_by_prio"] = flattenby("priority", tasks_array)
		tasks_json["todos_by_creation"] = flattenby("creation_date", tasks_array)
		tasks_json["todos_by_context"] = flattenby("contexts", tasks_array)
		tasks_json["todos_by_project"] = flattenby("projects", tasks_array)

	tasks_array = []


	return tasks_json

# flatten task list by ...
def flattenby(attribute, tasks):

	tasks_by = {}
	for task in tasks:
		if (attribute in task):
			if (type(task[attribute]) is list):
				for subattribute in task[attribute]:
					append_task(task, subattribute, tasks_by)
			else:
				append_task(task, task[attribute], tasks_by)
		else:
			append_task(task, KEY_EMPTY, tasks_by)

	return tasks_by

# append to list, create list if not existent
def append_task(task, key, container):
	if (key not in container):
		container[key] = []
	container[key].append(task)

# convert task to map
def task2map(task):

	mapped_task = {}
	if (task.description):
		mapped_task["description"] = task.bare_description()
	if (task.is_completed):
		mapped_task["is_completed"] = task.is_completed
	if (task.priority):
		mapped_task["priority"] = task.priority
	if (task.completion_date):
		mapped_task["completion_date"] = task.completion_date.isoformat()
	if (task.creation_date):
		mapped_task["creation_date"] = task.creation_date.isoformat()
	if (task.projects):
		mapped_task["projects"] = task.projects
	if (task.contexts):
		mapped_task["contexts"] = task.contexts
	if (task.attributes):
		mapped_task["attributes"] = task.attributes

	return mapped_task


# init args parser, read and save sources
if __name__ == "__main__":

	logging.basicConfig(format='%(asctime)s | %(levelname)s | %(message)s', datefmt='%Y-%m-%d | %H:%M:%S', level=logging.WARNING)

	parser = argparse.ArgumentParser(description = "Convert todo.txt to json.")
	parser.add_argument("-i", "--input", type = str, help = "input file", required = True)
	parser.add_argument("-o", "--output", type = str, help = "output file", required = True)
	parser.add_argument("-f", "--flatten", action='store_true', help = "flatten substructures", required = False)
	parser.add_argument("-v", "--verbose", action='store_true', help = "verbose output", required = False)

	args = parser.parse_args()

	try:
		if (args.verbose):
			logging.getLogger().setLevel(logging.INFO)

		todos = load_todotxt(args.input)
		tasks_json = todotxt2json(todos, args.flatten)

		output_file = args.output
		logging.info("Saving '%s'.", output_file)
		save_json_file(tasks_json, output_file)

	except Exception as e:
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		logging.error("%s in %s, line %d | %s", type(e).__name__, fname, exc_tb.tb_lineno, e)
